=====================================================================================
Můj vzkaz Američanům a přátelům z celého světa ohledně útoku na Kapitol v tomto týdnu
=====================================================================================

Arnold @Schwarzenegger (10. 1. 2021)

Jako člověk, který do této země imigroval, rád bych řekl svým americkým
spoluobčanům pár slov o událostech posledních dnů.

Evropská zkušenost ztráty demokracie 
=====================================

Vyrostl jsem v Rakousku.

Vím, co byla Kristallnacht, nebo-li Křišťálová noc - Noc rozbitého skla.

Byla to noc zuřivého běsnění proti Židům, kterou v roce 1938 provedl
nacistický ekvivalent Proud Boys.

Středa byla Dnem rozbitého skla právě tady, ve Spojených státech.

Rozbité sklo bylo v oknech Kapitolu Spojených států.

Ale dav nerozbil jen okna Kapitolu.

Útočníci rozbili myšlenky, které jsme považovali za samozřejmé.

Nevyrazili jen dveře budovy, ve které sídlila americká demokracie.

Pošlapali samotné principy, na nichž byla založena naše země.

Vyrostl jsem v troskách země, která utrpěla ztrátu své demokracie.

Narodil jsem se v roce 1947, dva roky po druhé světové válce.

Když jsem vyrůstal, byl jsem obklopen zlomenými muži, kteří v alkoholu
utápěli svou vinu za spoluúčast na nejhorším režimu v historii.

Ne všichni byli zuřivými antisemity nebo nacisty.

Mnozí jen postupně přijímali, krůček po krůčku, nastavenou cestu.

Byli to lidé odvedle.

Víte, nikdy jsem se s tímto nesvěřoval takto veřejně, protože je to
bolestná vzpomínka.

Ale můj otec přicházel jednou nebo dvakrát týdně domů opilý, křičel a
mlátil nás a děsil mou matku.

Nevnímal jsem, že by to byla zcela jeho vina, protože náš soused dělal
totéž své vlastní rodině, a stejně tak další soused.

Slyšel jsem to na vlastní uši a viděl jsem to na vlastní oči.

Trpěli fyzickou bolestí ze střepin ve svých tělech a emocionální bolestí
z toho, co viděli nebo dělali.

Všechno začalo lží, následovala další lež, pak další, společně s
nesnášenlivostí.

Protože pocházím z Evropy, poznal jsem na vlastní kůži, jak se věci
mohou vymknout z kloubů.

Dnešní ohrožení demokracie v Americe 
=====================================

Vím, že v této zemi a na celém světě panuje strach, že by se něco
takového mohlo stát právě tady.

Nedomnívám se, že k tomu dojde, ale věřím, že si musíme uvědomit ty
katastrofální důsledky sobectví a cynismu.

Prezident Trump se pokoušel zvrátit výsledky voleb, a to spravedlivých
voleb.

Pokusil se o puč tím, že lidi obelhal a svedl na scestí.

Můj otec i vaši sousedé byli také obelháni a svedeni, a já vím, kam
takové lži vedou.

Prezident Trump je neúspěšný vůdce.

Zapíše se do historie jako vůbec nejhorší prezident.

Dobrá věc je, že brzy bude stejně nedůležitý jako starý tweet.

Co s těmi, co lži a zradu umožňují 
===================================

Co si ale počít s těmi zvolenými zástupci, kteří mu tyto lži a zradu
umožnili?

Připomenu jim, co řekl Teddy Roosevelt: „Vlastenectví znamená postavit
se za vlast. Neznamená to postavit se za prezidenta.“

John F. Kennedy napsal knihu s názvem Profily odvahy.

Řada členů mé vlastní strany by kvůli své bezpáteřnosti v této knize svá
jména nikdy neviděla. To vám garantuji.

Jsou spolupachateli těch, kteří nesli vlajku arogantního útoku na
Kapitol.

Ale nezafungovalo to.

Naše demokracie zůstala pevná.

Stačilo pár hodin, Senát a Sněmovna reprezentantů jednaly v zájmu občanů
a potvrdily volbu nově zvoleného prezidenta Bidena.

Jaký to skvělý projev demokracie!

Potřebujeme "srdce služebníka" 
===============================

Vyrostl jsem jako katolík, chodil jsem do kostela, docházel do katolické
školy, učil se Bibli a katechismus.

A z těchto dnů si pamatuji slovní spojení, které je dnes aktuální,
„srdce služebníka.“

Znamená to sloužit něčemu většímu, než jsme my sami.

To, co právě teď potřebujeme od našich volených zástupců, je „srdce
služebníka.“

Potřebujeme veřejné zástupce, kteří slouží něčemu většímu, než je jejich
vlastní moc, nebo jejich vlastní strana.

Potřebujeme veřejné zástupce, kteří chtějí sloužit vyšším ideálům,
myšlenkám, na kterých byla tato země založena, ideálům, k nimž ostatní
země vzhlížejí.

Svět se nás dívá 
=================

Teď, v posledních několika dnech, mi volali přátelé z celého světa,
jeden telefonát za druhým, a volali vyděšení, s obavami o nás jako
národ.

Jedna žena ronila slzy pro Ameriku, překrásné slzy ideální představy o
tom, jaká by Amerika měla být.

Tyto slzy by nám měly připomínat, co Amerika znamená pro svět.

A já jsem řekl každému, kdo volal: jakkoliv nám aktuální situace rve
srdce, Amerika se z těchto temných dnů probere a znovu nám zazáří.

Conanův meč a demokracie 
=========================

Vidíte tento meč? Toto je Conanův meč.

S meči se to má tak, že čím víc je kalíte, tím jsou pevnější.

Čím víc do něj bušíte kladivem, a zahříváte ho v ohni, a pak ho vrazíte
do studené vody, a pak do něj bušíte znovu, a pak ho vrazíte do ohně a
do vody, čím častěji to děláte, tím pevnější se stává.

Neříkám vám to všechno proto, abych z vás učinil zkušené mečíře, ale
proto, že naše demokracie je jako ocel tohoto meče.

Čím více se kalí, tím se stává pevnější.

Naše demokracie byla kalena válkami, nespravedlností a vzpourami.

Věřím, že tak, jako jsme dnes otřeseni událostmi posledních dnů, tak
stejně tak vyjdeme posíleni, protože nyní už chápeme, co můžeme ztratit.

Demokracie jako sjednocující zájem nás všech 
=============================================

Potřebujeme reformy, samozřejmě, aby se toto už nikdy neopakovalo.

Musíme pohnat k zodpovědnosti ty, kteří nás přivedli až do této
neomluvitelné situace.

A musíme odhlédnout od nás samotných, od našich stran a od našich
neshod, a dát na první místo naši demokracii.

A musíme se uzdravit - všichni společně - z dramatu toho, co se právě
stalo.

Musíme se uzdravit, ne jen jako Republikáni nebo Demokraté - ale jako
Američané.

Nyní, abychom zahájili tento proces, vás žádám, abyste se - bez ohledu
na vaše stranické preference - připojili k mému vzkazu nově zvolenému
prezidentu Bidenovi:

„Zvolený prezidente Bidene, přejeme Vám, jako našemu prezidentovi, velký
úspěch. Pokud uspějete, uspěje náš národ. Podporujeme Vás celým svým
srdcem ve Vaší snaze nás sjednotit.“

A vy, co si myslíte, že můžete pokořit Ústavu Spojených států, vězte
toto: Nikdy nevyhrajete!

„Zvolený prezidente Bidene, stojíme s Vámi dnes, zítra a navždy při
obraně naší demokracie před těmi, kdo by ji ohrožovali.“

Ať Bůh žehná Vám všem a ať Bůh žehná Americe.

| Verze 2 (12.1. 2021) Zdroj:
  https://twitter.com/Schwarzenegger/status/1348249481284874240
| Přeložil a o nadpisy doplnil @JanVLC, korektury Ivona Vlčinská

Viz též: https://www.youtube.com/watch?v=bXwSLPlTdZc
